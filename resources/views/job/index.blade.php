@extends('template.index')
@section('content')
    
    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="career">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1 style="color: #000;">
                            Open Positions
                        </h1><br>
                        <h4 style="color: #000;">
                            WE cares and nurtures our people
                        </h4>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
      </section>
      <!--================ Start Contact Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: -50px">
            <div class="row">
                <!-- job 1 -->
                <div class="col-lg-4 col-md-6 col-sm-12">
                  <div class="jobs-des">
                    <div class="m-title">
                      <h4>
                        <a href="{{ url('/Sales-Executive')}}">
                          SALES EXECUTIVE
                        </a>
                      </h4>
                      <p><i class="fas fa-map-marker-alt"></i> Jakarta</p>
                    </div>
                  </div>
                </div>
                <!-- job 2 -->
                <div class="col-lg-4 col-md-6 col-sm-12">
                  <div class="jobs-des">
                    <div class="m-title">
                      <h4>
                        <a href="{{ url('/Engineer')}}">
                          ENGINEER
                        </a>
                      </h4>
                      <p><i class="fas fa-map-marker-alt"></i> Jakarta</p>
                    </div>
                  </div>
                </div>
                <!-- job 3 -->
                <div class="col-lg-4 col-md-6 col-sm-12">
                  <div class="jobs-des">
                    <div class="m-title">
                      <h4>
                        <a href="{{ url('/Architect')}}">
                          ARCHITECT
                        </a>
                      </h4>
                      <p><i class="fas fa-map-marker-alt"></i> Jakarta</p>
                    </div>
                  </div>
                </div>
                <!-- job 4 -->
                <div class="col-lg-4 col-md-6 col-sm-12">
                  <div class="jobs-des">
                    <div class="m-title">
                      <h4>
                        <a href="{{ url('/Sustainable-Engineer')}}">
                          SUSTAINABLE ENGINEER
                        </a>
                      </h4>
                      <p><i class="fas fa-map-marker-alt"></i> Jakarta</p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Contact Area =================-->
    @endsection