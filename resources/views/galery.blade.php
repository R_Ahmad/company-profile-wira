@extends('template.index')
@section('content')
    
    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="gallery">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            What we've done
                        </h1><br>
                        <h4>
                            WE's innovative solutions help Electricity, Gas and Water Utilities mprove their top line by creating differentiated offerings and processes that involve the consumer.
                        </h4>
                    </div>
                </div><!-- 
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div> -->
            </div>
        </div>
      </section>
        <section class="mbr-gallery mbr-slider-carousel cid-ruuHhHl8AI" id="gallery2-0">
          <div class="container">
              <div><!-- Filter -->
                <div class="mbr-gallery-filter container gallery-filter-active">
                  <ul buttons="0">
                    <li class="mbr-gallery-filter-all">
                      <a class="btn btn-md btn-primary-outline active display-7" href="">All</a>
                    </li>
                  </ul>
                </div>
                <!-- Gallery -->
                  <div class="mbr-gallery-row">
                    <div class="mbr-gallery-layout-default">
                      <div>
                        <div>
                          <!-- Natural Gas -->
                          <!-- image 1 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="0" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8315.jpg') }}" alt="" title="">
<!--                                 <span class="icon-focus">
                                  <div class="title">Tittle</div>
                                  <p>
                                    Call me Ishmael. Some years ago—never mind how long precisely—having little
                                  </p>
                                </span> -->
                            </div>
                          </div>
                          <!-- image 2 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="1" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8322.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 3 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="2" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8329.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 4 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="3" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8336.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 5 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="4" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8342.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 6 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="5" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8346.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 7 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="6" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8357.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 8 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="7" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8370.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 9 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="8" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8191.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 10 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="9" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8203.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 11 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="10" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8211.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 12 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="11" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8218.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 13 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="12" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8230.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 14 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="13" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8241.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 15 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="14" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8253.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 16 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="15" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8274 copy.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 17 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="16" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8276.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 18 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="17" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8278.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 19 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="18" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8280.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 20 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="19" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8289.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 21 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="20" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8299.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 22 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="21" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8302.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 23 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="22" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8305.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 24 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="23" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8307.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          <!-- image 25 -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Natural Gas">
                            <div href="#lb-gallery2-0" data-slide-to="24" data-toggle="modal">
                              <img src="{{ asset('template/img/NG Retail/IMG_8310.jpg') }}" alt="" title="">
                            </div>
                          </div>
                          

                          <!-- Electricity -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Electricity">
                            <div href="#lb-gallery2-0" data-slide-to="25" data-toggle="modal">
                              <img src="{{ asset('template/img/img-electricity.png') }}" alt="" title="">
                            </div>
                          </div>

                          <!-- water trwatment -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Water Treatment">
                            <div href="#lb-gallery2-0" data-slide-to="26" data-toggle="modal">
                              <img src="{{ asset('template/img/service/water-treatment/13.png') }}" alt="" title="">
                            </div>
                          </div>

                          <!-- Waste Water Treatment -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Waste Water Treatment">
                            <div href="#lb-gallery2-0" data-slide-to="27" data-toggle="modal">
                              <img src="{{ asset('template/img/service/waste-water/14.png') }}" alt="" title="">
                            </div>
                          </div>

                          <!-- High Speed Diesel -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="High Speed Diesel">
                            <div href="#lb-gallery2-0" data-slide-to="28" data-toggle="modal">
                              <img src="{{ asset('template/img/PastedGraphic-2.jpg') }}" alt="" title="">
                            </div>
                          </div>

                          <!-- Waste To Energy -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Waste To Energy">
                            <div href="#lb-gallery2-0" data-slide-to="29" data-toggle="modal">
                              <img src="{{ asset('template/img/service/waste-to-energy/wasteprocess.png') }}" alt="" title="">
                            </div>
                          </div>

                          <!-- Photovoltaic -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Photovoltaic">
                            <div href="#lb-gallery2-0" data-slide-to="30" data-toggle="modal">
                              <img src="{{ asset('template/img/service/photovoltaict/pvproducts.png') }}" alt="" title="">
                            </div>
                          </div>

                          <!-- Internet Of Things (IOT) -->
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="31" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/Meter Listrik.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="32" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/Meter Air.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="33" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/Meter Gas.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="34" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/Reed Switch.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="35" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/Wirnet IBTS.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="36" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/iStation.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="37" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/iFemto.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="38" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/Non mag Sensor.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="39" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/RTU.png') }}" alt="" title="">
                            </div>
                          </div>
                          <div class="mbr-gallery-item mbr-gallery-item--p2" data-video-url="false" data-tags="Internet of Things">
                            <div href="#lb-gallery2-0" data-slide-to="40" data-toggle="modal">
                              <img src="{{ asset('template/img/IOT Produk/Temp Sensor.png') }}" alt="" title="">
                            </div>
                          </div>
                          
                        </div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                  <!-- Lightbox -->
                  <div data-app-prevent-settings="" class="mbr-slider modal fade carousel slide" tabindex="-1" data-keyboard="true" data-interval="false" id="lb-gallery2-0">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-body">
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8315.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8322.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8329.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8336.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8342.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8346.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8357.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Fixed Storage/IMG_8370.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8191.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8203.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8211.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8218.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8230.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8241.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Pipa/IMG_8253.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8274 copy.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8276.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8278.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8280.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8289.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8299.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8302.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8305.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8307.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/NG Retail/IMG_8310.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/img-electricity.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/service/water-treatment/13.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/service/waste-water/14.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/PastedGraphic-2.jpg') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/service/waste-to-energy/wasteprocess.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/service/photovoltaict/pvproducts.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/Meter Listrik.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/Meter Air.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/Meter Gas.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/Reed Switch.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/Wirnet IBTS.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/iStation.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/iFemto.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/Non mag Sensor.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/RTU.png') }}" alt="" title="">
                            </div>
                            <div class="carousel-item">
                              <img src="{{ asset('template/img/IOT Produk/Temp Sensor.png') }}" alt="" title="">
                            </div>
                            
                            </div>
                            <a class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#lb-gallery2-0">
                              <!-- <span class="mbri-left mbr-iconfont" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span> -->
                              <i class="fas fa-chevron-circle-left fa-2x"></i>
                            </a>
                            <a class="carousel-control carousel-control-next" role="button" data-slide="next" href="#lb-gallery2-0">
                              <!-- <span class="mbri-right mbr-iconfont" aria-hidden="true"></span>
                              <span class="sr-only">Next</span> -->
                              <i class="fas fa-chevron-circle-right fa-2x"></i>
                            </a>
                            <a class="close" href="#" role="button" data-dismiss="modal">
                              <i class="far fa-times-circle fa-2x"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
<!--================ Start Client Area =================-->
  <section class="testimonial-area section-gap">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-10 text-center">
          <div class="section-title">
            <h2>Our Client</h2>
            <h4>
              Over 50+ clients in less than 3 years. Some of them are listed below
            </h4>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-lg-10 text-center">
          <div class="owl-carousel active-testi-carousel">
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Fixed Storage/Lippo Mall Cikarang.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Fixed Storage/Maxxbox Lippo Vilage.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Fixed Storage/Pisang Madu bu Nanik.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Fixed Storage/pt star mustika plastmetal2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Fixed Storage/Sate Khas Senayan.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Fixed Storage/Space Soewarna.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Pipa/Alam Sutera2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Pipa/Flavour Bliss Alsut.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Pipa/Lippo Mall Puri.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Pipa/Lippo Plaza Ekalokasari Bogor2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Pipa/Plaza Semanggi.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Pipa/Puri Indah Mall1.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Pipa/Setia Budi One.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Bakso Afung2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Casablanca Catering2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Indocarter2.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Khubilai Khan2.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Nia Catering2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Pancoran China Town2.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Papa Jack2.jpg')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Rooftop West2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Tumpeng Mini2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Wang Plaza2.png')}}" alt=""></div>
              </div>
            </div>
            <!-- single carousel -->
            <div class="single-testi-item">
              <div class="author-title">
                <div class="thumb"><img src="{{asset('template/img/client/Logo Client Retail/Waroeng Sunda2.jpg')}}" alt=""></div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--================ End Client Area =================-->
    @endsection