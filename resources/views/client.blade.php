@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="client">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Our Clients
                        </h1><br>
                        <h3>
                            Over 50+ clients in less than 3 years. Some of them are listed below
                        </h3>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row" style="text-align: center;">
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b1.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b1.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b2.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b2.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b3.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b3.png')}}" style="width: 60%;">
                </div>
            </div>
            <div class="row" style="margin-top: 20px;;text-align: center;">
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b4.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b4.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b1.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b2.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b3.png')}}" style="width: 60%;">
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6" style="margin: auto;">
                    <img src="{{ asset('template/img/brands/b4.png')}}" style="width: 60%;">
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->