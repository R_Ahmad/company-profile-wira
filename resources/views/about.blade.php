@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
        <section class="service-banner-area" id="about-us">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 home-banner-left d-flex fullscreen align-items-center">
                	<div style="margin: auto;">
                        <h1 class="" style="color: #fff;">
                            About Wira Energi
                       </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding home-banner-right d-flex fullscreen align-items-end">
                </div>
            </div>
        </div>
        </section>
    <!--================ End banner Area =================-->
    <!--================ Start Private Utilities Company Area =================-->
    <section>
        <div class="container" style="margin-top: 20px;">
            <div class="row">
                <div class="col-lg-6 col-md-6" style="text-align: center;margin: auto;">
                    <img src="{{asset('template/img/we-sign.png')}}" style="width: 80%;height: auto;">
                </div>
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <h1 style="color: #429C46">
                    	Private Utilities Company
                    </h1><br>
                    <p style="text-align: justify;">
                        WIRA ENERGI (WE) was established in 2013 as a private utility corporation whose primary mission is to support Local Developer Corporation’s efforts to stimulate economic growth at Industrial Park, Commercial and Residential ith commitment to safety, reliability and system efficiency. WE is the utility that sells electricity, steam energy, natural gas and clean water to the industries and commercial. WE is responsible for all maintenance and capital improvements to the natural gas, utility electric and steam distribution systems of the park. WE also responds to any interruptions in service with utility crews that are on-call and available 24/7
                    </p><br>
                </div>
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <h1 style="color: #429C46">
                    	Utility
                    </h1><br>
                    <p style="text-align: justify;">
                        Utility companies continue to focus more on customer engagements, ever-changing regulations and integration of renewable energy. There is also an effort to optimize the energy mix, deployment of smart technology, intelligent investment for operational efficiency. A need to align to greater competition and regulatory directives becomes imperative as cost of materials and labor go up. Utilities are, thus, taking a fresh look at technology as a tool to gain competitive advantage.
                    </p><br>
                </div>
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <h1 style="color: #429C46">
                    	Service
                    </h1><br>
                    <p style="text-align: justify;">
                        WE provides the most reliable and economical energy and water solutions to our present and future customers in Residential, Commercial and Industrial Area. 
                        Partner in managing your energy requirement and improving your value of the business, saving the energy cost without or minor spending any additional investment and guaranteed reliability. Our services covering your energy need as natural gas, electricity, cooling and steam, diesel (HSD), Internet Of Things (IOT) and clean water, to support your core business
					</p><br>
                </div>
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <h1 style="color: #429C46">
                    	How WE helps
                    </h1><br>
                    <p style="text-align: justify;">
                    	WE's innovative solutions help Electricity, Gas and Water Utilities improve their top line by creating differentiated offerings and processes that involve the consumer.
                    </p>
                    <p>
                    	We offer:
                    </p>
                    <ul style="list-style-type:disc;margin-left: 30px;">
                    	<li> Enterprise solutions to manage work and assets</li>
                    	<li>Energy management systems to optimize and integrate sources</li>
                    	<li>Integration and consolidation of processes to harness efficiencies</li>
                    	<li>Bringing in best practices that enhance reliability and security</li>
                    	<li>Monitoring system</li>
                    </ul>
                    <p>
                    	Some of our innovative domain solutions and business transformation accelerators for Electricity and Gas Utilities are:
                    </p>
                    <ul style="list-style-type:disc;margin-left: 30px;">
                    	<li>Generation & Renewabless</li>
                    	<li>Transmission & Distribution</li>
                    	<li>Smart Metering and Smart Grid Technologies</li>
                    	<li>Customer Billing & Retail</li>
                    	<li>Energy Trading & Risk Management</li>
                    	<li>Environment, Health and Safety</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6">
                    <h1 style="color: #429C46">
                    	Market
                    </h1><br>
                    <p style="text-align: justify;">
                        WE is the right partner for property’s developers in managing their clients’ energy need like natural gas and electricity in Industrial estates/parks, commercials like malls, restaurants etc. and residential like apartments. WE can build and operate the power plant and steam generator for your needs and let you be focus with your main business.
                    </p><br>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Private Utilities Company Area =================-->
@endsection