@extends('template.index')
@section('content')
    
    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="kontak">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Contact Us
                        </h1><br>
                        <h4>
                            Let's talk bussiness or just have a cofee, We would love to hear from you!
                        </h4>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
      </section>
      <!--================ Start Contact Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-5 col-md-5" style="margin: auto;">
                    <p>
                        Soho Podomoro City (NEO SOHO) Lantai 40 Unit 09, Jl Let. Jend. S. Parman Kav 28 Jakarta Barat 11470 Indonesia
                    </p>
                    <div class="row">
                      <ul class="col-lg-3 col-md-3 col-sm-3 footer-nav">
                        <li>Phone</li>
                        <li>Fax</li>
                        <li>Email</li>
                      </ul>
                      <ul class="col-lg-9 col-md-9 col-sm-9 footer-nav">
                        <li><a href="tel:+622129181308">: +62 21 2918 1308</a></li>
                        <li><a href="fax:+622129181308">: +62 21 2918 1308</a></li>
                        <li><a href="mailto:info@wiraenergi.co.id?subject=Testing link mail">: info@wiraenergi.co.id</a></li>
                      </ul>
                        </div>
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.6712174122513!2d106.78748541444676!3d-6.174752862226197!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f65ee1c07a31%3A0x2c4c67f4d94e8e65!2sNEO%20SOHO%20Central%20Park!5e0!3m2!1sid!2sid!4v1573626152927!5m2!1sid!2sid" width="100%" height="200px" frameborder="0" style="border:0;" allowfullscreen=""></iframe> 
                </div>
                <div class="col-lg-7 col-md-7" style="">
                    <form action="{{ url('/sendEmail') }}" method="post">
                      {{ csrf_field() }}
                      @if(\Session::has('alert-failed'))
                                    <div class="alert alert-failed" style="float: left;">
                                        <div>{{Session::get('alert-failed')}}</div>
                                    </div>
                                @endif
                                @if(\Session::has('alert-success'))
                                    <div class="alert alert-success" style="float: left;">
                                        <div>{{Session::get('alert-success')}}</div>
                                    </div>
                                @endif
                      <div class="row">
                        <div class="col-lg-6 col-md-6">
                          <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                          </div>
                          <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                          </div>
                          <div class="form-group">
                            <label for="">Subject</label>
                            <input type="text" class="form-control" id="judul" name="judul">
                          </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                          <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" rows="4" id="pesan" name="pesan"></textarea>
                          </div>
                          <button type="Submit" style="color: #429C46;float: right;" class="btn btn-default btn-sm">Send Message</button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Contact Area =================-->
    @endsection