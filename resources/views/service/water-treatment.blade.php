@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="water-treatment">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Water Treatment
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="title">
                        Clean drinking water at home (Apartment)
                    </div><br>
                    <p style="text-align: justify;">
                        WE could offer the Drinkable Water Treatment facilities as, EPC (EngineeringProcurement and Construction), BOT (Build Operate and Transfer) or BOO(Build Operate and Owned) The Drinkable Water charge will have a minimumcharge and as used according to the meterWE offers online drinking water in m3 by installing the complete unit and readyto deploy, easy to install and to operate, designed for in-house application andis a perfect solution for hospitals, schools, camps or any other public institutions.
                    </p>
                    <p style="text-align: justify;">
                        99.9999% virus-and-bacteria free thanks to innovative filtration technology – ultrafiltration, nearly no maintenance costs
                    </p>
                    <p>
                        Our solutions offers you:
                        <ul style="list-style-type:disc;margin-left: 30px;">
                            <li> Optimized Management of Water.</li>
                            <li> Modular Design for Environmental Footprint Reduction.</li>
                            <li> Lower Operating & Maintenance Costs.</li>
                            <li>Simple Integration into existing and new facilities.</li>
                            <li>Regulatory Compliance.</li>
                        </ul>
                    </p>
                    <p style="text-align: justify;">
                        WE Genesis Water Technologies provides maximum flexibility and value for our clients and local partners to assist them in dealing with environmental compliance issues and the challenges associated with water scarcity.
                    </p>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                    <img src="{{asset('template/img/service/water-treatment/13.png')}}" style="width: 80%;height: auto;">
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
@endsection