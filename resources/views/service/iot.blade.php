@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="iot">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Internet of Things
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6" style="">
                    <img src="{{asset('template/img/service/iot/iot.jpg')}}" style="width: 80%;height: auto;">
                </div>
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <div class="title">
                        Internet of Things
                    </div><br>
                    <p style="text-align: justify;">
                        Wira Energi is private utility company that is focusing on utilities supply. We encounter internal difficulties by using  traditional utility meter from manual reading, manual billing, payment collection, and manual controlling. To address this current problem we inovate solutions which is IoT prepaid utility (gas, water, electricity) meter system. Our IoT meter system is using LoraWan technology. This technology help us to remote monitoring, remote top-up, analyzing, controlling, automated billing and payment collection. By having this solution many companies and building operator are interested in our solution. So now we’re concerning to roll out this solution to all over the world in bringing great solutions for all.
                    </p>
                </div>
                <div class="col-lg-12 col-md-12" style="margin: 20 auto;text-align: center;">
                     <!-- <video width="100%" height="auto" controls>
                      <source src="{{ asset('template/img/WE Convenient Meter Video.MP4')}}" type="video/mp4">
                      <source src="movie.ogg" type="video/ogg">
                    Your browser does not support the video tag.
                    </video>  -->
                    <a href="https://youtu.be/C0QwWZvH97E" target="_blank">
                            <img src="{{ asset('template/img/poster2.png')}}" style="width: 100%;height: auto;">
                          </a>
                </div>
                <div class="col-lg-6 col-md-6" style="margin: ;">
                    <div class="title">
                        Smart Meter
                    </div><br>
                    <p style="text-align: justify;">
                        The Smart Meter is supporting your gas, electric and water supply to tenants
                    </p>
                    <p style="text-align: justify;">
                        Simplify your administration to check the use of the gas, sent the bills and receipt, to pay or collecting the payment using your smart phones.
                    </p>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
<!--                     <img src="{{asset('template/img/service/iot/Meter Air.png')}}" style="width: 80%;height: auto;">

                    <img src="{{asset('template/img/service/iot/Meter Gas.png')}}" style="width: 80%;height: auto;">
 -->
                    <img src="{{asset('template/img/service/iot/Meter Listrik.png')}}" style="width: 50%;height: auto;"><img src="{{asset('template/img/service/iot/Meter Air.png')}}" style="width: 50%;height: auto;">
                    <img src="{{asset('template/img/service/iot/Meter Gas.png')}}" style="width: 50%;height: auto;">
                </div>
                
             </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
@endsection