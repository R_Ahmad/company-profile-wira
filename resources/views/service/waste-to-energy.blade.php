@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="waste-to-energy">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Waste To Energy
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="title">
                        Incinerator & Pyrolysis Waste Management Solution
                    </div><br>
                    <p style="text-align: justify;">
                        Waste is a problem that represents high costs to governments, citizens, companies, and has a strong environmental effect on our planet. WE is focused on allowing organizations to utilize their waste and empower our planet. Our proprietary and advanced technology allows for an unmatched result: to convert a variety of waste types into energy at a remarkably high rate. Our process is highly energy efficient, and it produces no pollution. Our equipment allows our customers to eliminate expenses on waste-disposal, produce electricity and operate in a highly profitable way.
                    </p><br>
                    <div class="title">
                        Let's clean our planet
                    </div>
                    <p>
                        WE's unique proposal:
                        <ul style="list-style-type:disc;margin-left: 30px;">
                            <li> Invest and build Waste Processing Plant Facilities are safe and environmentally friendly with promising employment opportunities.</li>
                            <li>Reduce carbon emissions and pollution garbage.</li>
                            <li>Meet the requirement of power supply and diesel.</li>
                            <li>By recovering energy from unused waste into useful materials, WE brings Pemda an advantageous in managing Landfill</li>
                        </ul>
                    </p><br>
                    <div class="title">
                        Sequence Processing and Waste Destruction
                    </div><br>
                    <p>
                        <img src="{{asset('template/img/service/waste-to-energy/wasteprocess.png')}}" style="width: 100%;height: auto;">
                    </p>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                    <img src="{{asset('template/img/service/waste-to-energy/18.png')}}" style="width: 80%;height: auto;">
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
@endsection